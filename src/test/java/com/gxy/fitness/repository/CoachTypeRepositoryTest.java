package com.gxy.fitness.repository;


import com.gxy.fitness.entity.CoachType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author GUO
 * @Classname BarberTypeRepositoryTest
 * @Description TODO
 * @Date 2021/12/11 15:42
 */
@SpringBootTest
@Slf4j
public class CoachTypeRepositoryTest {

    @Autowired
    private BarberTypeRepository barberTypeRepository;
    @Test
    public void findByBarberTypeId() {
        Long id=123456l;
        CoachType byCoachType = barberTypeRepository.findByBarberTypeId(id);
        log.info(byCoachType.toString());
    }
}
