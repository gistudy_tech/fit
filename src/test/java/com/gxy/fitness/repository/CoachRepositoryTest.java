package com.gxy.fitness.repository;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author GUO
 * @Classname BarberRepositoryTest
 * @Description TODO
 * @Date 2022/2/26 16:11
 */
@SpringBootTest
@Slf4j
public class CoachRepositoryTest {
    @Autowired
    private CoachRepository coachRepository;
    @Test
    public void countByBarberTypeId() {
        int count = coachRepository.countByBarberTypeId(7799343901590528l);
        log.info(String.valueOf(count));
    }
}
