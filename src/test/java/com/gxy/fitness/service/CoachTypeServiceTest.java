package com.gxy.fitness.service;


import com.gxy.fitness.req.BarberTypeReq;
import com.gxy.fitness.resp.CoachTypeResp;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author GUO
 * @Classname BarberTypeServiceTest
 * @Description TODO
 * @Date 2021/12/11 15:50
 */
@SpringBootTest
@Slf4j
public class CoachTypeServiceTest {

    @Autowired
    private CoachTypeService coachTypeService;
    @Test
    public void save() {
        BarberTypeReq barberTypeReq=new BarberTypeReq();
//        barberTypeReq.setBarberTypeId(850090465346522l);
//        BigDecimal price = BigDecimal.valueOf(30.00);
//        barberTypeReq.setBarberPrice(price);
        barberTypeReq.setBarberTypeName("理发师");
        coachTypeService.save(barberTypeReq);
    }
    @Test
    public void list() {
        List<CoachTypeResp> typeRespList = coachTypeService.list();
        log.info(typeRespList.toString());
    }
}