package com.gxy.fitness.repository;

import com.gxy.fitness.entity.Coach;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author GUO
 * @Classname OrderRepository
 * @Description TODO
 * @Date 2021/12/8 19:48
 */
public interface CoachRepository extends JpaRepository<Coach,Long> {
    int countByBarberTypeId(Long barberTypeId);
    Coach findByBarberId(Long barberId);
    List <Coach> findByBarberTypeId(Long barberTypeId);
}
