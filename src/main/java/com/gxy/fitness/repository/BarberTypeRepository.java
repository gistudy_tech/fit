package com.gxy.fitness.repository;

import com.gxy.fitness.entity.CoachType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author GUO
 * @Classname OrderRepsitory
 * @Description TODO
 * @Date 2021/12/8 19:48
 */
public interface BarberTypeRepository extends JpaRepository<CoachType,Long> {
    CoachType findByBarberTypeId(Long barberTypeId);
}
