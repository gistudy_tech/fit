package com.gxy.fitness.repository;

import com.gxy.fitness.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author GUO
 * @Classname HairsRepository
 * @Description TODO
 * @Date 2021/12/8 19:48
 */
public interface ProjectRepository extends JpaRepository<Project,Long> {

    int countByHairTypeId(Long hairTypeId);

    List<Project> findByHairTypeId(Long hairTypeId);

    List<Project> findByBarberTypeId(Long barberTypeId);

    List<Project> findByHairNameLike(String hairName);
    Project findByHairId(Long hairId);
}
