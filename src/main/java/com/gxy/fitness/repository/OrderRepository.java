package com.gxy.fitness.repository;

import com.gxy.fitness.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author GUO
 * @Classname OrderRepsitory
 * @Description TODO
 * @Date 2021/12/8 19:48
 */
@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findByUserId(Long userId);
    Order findByOrderId(Long orderId);
    List<Order> findByPhone(String phone);
}
