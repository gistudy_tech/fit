package com.gxy.fitness.controller;

import com.gxy.fitness.form.UserForm;
import com.gxy.fitness.req.UserReq;
import com.gxy.fitness.resp.CommonResp;
import com.gxy.fitness.resp.PageResp;
import com.gxy.fitness.resp.UserResp;
import com.gxy.fitness.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author GUO
 * @Classname UserController
 * @Description TODO
 * @Date 2021/12/8 13:59
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("/list")
    public CommonResp userList(@Valid UserReq userReq){
        CommonResp<PageResp<UserResp>> resp=new CommonResp<>();
        PageResp<UserResp> pageResp = userService.list(userReq);
        resp.setContent(pageResp);
        return resp;
    }
    @PostMapping("/save")
    public CommonResp userSave(@Valid @RequestBody UserForm userForm){

        CommonResp resp=new CommonResp();
        resp.setMessage("更新个人信息");
        userService.save(userForm);
        return resp;
    }
    @DeleteMapping("/userDel/{userId}")
    public CommonResp userDel(@PathVariable Long userId){
        CommonResp resp=new CommonResp();
        resp.setMessage("删除用户成功");
        userService.userDel(userId);
        return resp;
    }
    @GetMapping ("/integral/{userId}")
    public CommonResp integral(@PathVariable Long userId){
        CommonResp resp=new CommonResp();
        Integer integral = userService.integral(userId);
        resp.setContent(integral);
        return resp;
    }
    @GetMapping("/findUser/{phone}")
    public CommonResp userInfo(@PathVariable String phone) {
        CommonResp resp=new CommonResp();
        UserResp userResp = userService.findByPhone(phone);
        resp.setContent(userResp);
        return resp;
    }

}
