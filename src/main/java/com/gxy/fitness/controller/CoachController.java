package com.gxy.fitness.controller;

import com.gxy.fitness.entity.Coach;
import com.gxy.fitness.form.BarberForm;
import com.gxy.fitness.req.BarberReq;
import com.gxy.fitness.resp.*;
import com.gxy.fitness.service.CoachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * @author GUO
 * @Classname BarberController
 * @Description TODO
 * @Date 2022/2/26 18:02
 */
@RestController
@RequestMapping("/coach")
public class CoachController {

    @Autowired
    private CoachService coachService;

//    @GetMapping("/index")
//    public CommonResp index() {
//        CommonResp<PageResp<BarberResp>> resp = new CommonResp<>();
//        PageResp<BarberResp> pageResp = coachService.list();
//        resp.setContent(pageResp);
//        return resp;
//    }

    @GetMapping("/list")
    public CommonResp userList(@Valid BarberReq coachReq) {
        CommonResp<PageResp<CoachResp>> resp = new CommonResp<>();
        PageResp<CoachResp> pageResp = coachService.list(coachReq);
        resp.setContent(pageResp);
        return resp;
    }
    @PostMapping("/save")
    public CommonResp coachTypeSave(@Valid @RequestBody BarberForm coachForm) {
        CommonResp resp = new CommonResp();
        coachService.save(coachForm);
        return resp;
    }

    @DeleteMapping("/delete/{coachId}")
    public CommonResp coachDel(@PathVariable Long coachId) {
        CommonResp resp = new CommonResp();
        resp.setMessage("删除理发师类型");
        coachService.del(coachId);
        return resp;
    }

    @GetMapping("/find/{coachId}")
    public CommonResp findById(@PathVariable Long coachId) {

        CommonResp resp = new CommonResp();
        CoachResp coachResp = coachService.findById(coachId);
        if (!ObjectUtils.isEmpty(coachResp)) {
            resp.setContent(coachResp);
            resp.setMessage("查询成功");
            return resp;
        } else {
            resp.setMessage("未查询到数据,请检查请求数据");
            resp.setSuccess(false);
            return resp;
        }
    }
    @GetMapping("/findHairId/{hairId}")
    public CommonResp findHairId(@PathVariable Long hairId) {
        CommonResp resp = new CommonResp();
        List<Coach> coachList = coachService.findHairId(hairId);
        resp.setContent(coachList);
        return resp;
    }
    @GetMapping("/findTypeId/{coachTypeId}")
    public CommonResp findTypeId(@PathVariable Long coachTypeId) {
        CommonResp resp = new CommonResp();
        List<Coach> coachList = coachService.findTypeId(coachTypeId);
        resp.setContent(coachList);
        return resp;
    }

}
