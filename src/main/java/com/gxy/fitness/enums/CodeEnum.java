package com.gxy.fitness.enums;

/**
 * @author GUO
 * @Classname CodeEnum
 * @Description TODO
 * @Date 2021/5/4 18:47
 */
public interface CodeEnum {
    Integer getCode();
}
