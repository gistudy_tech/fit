package com.gxy.fitness.service;

import com.gxy.fitness.entity.CoachType;
import com.gxy.fitness.entity.Project;
import com.gxy.fitness.entity.HairType;
import com.gxy.fitness.exception.BusinessException;
import com.gxy.fitness.exception.BusinessExceptionCode;
import com.gxy.fitness.form.HairForm;
import com.gxy.fitness.repository.BarberTypeRepository;
import com.gxy.fitness.repository.ProjectRepository;
import com.gxy.fitness.repository.HairTypeRepository;
import com.gxy.fitness.req.HairReq;
import com.gxy.fitness.resp.HairResp;
import com.gxy.fitness.resp.PageResp;
import com.gxy.fitness.utils.CopyUtil;
import com.gxy.fitness.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Date;
import java.util.Collections;
import java.util.List;

/**
 * @author GUO
 * @Classname HairsService
 * @Description TODO
 * @Date 2022/2/26 17:40
 */
@Slf4j
@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private HairTypeRepository hairTypeRepository;
    @Autowired
    private BarberTypeRepository barberTypeRepository;
    @Autowired
    private SnowFlake snowFlake;

    //微信小程序
    public List<HairResp> All(){
        List<Project> hairList = projectRepository.findAll();
        //拷贝工具类
        List<HairResp> respList = CopyUtil.copyList(hairList, HairResp.class);
        for (HairResp hairResp:respList){
            HairType hairType = hairTypeRepository.findByHairTypeId(hairResp.getHairTypeId());
            CoachType coachType =barberTypeRepository.findByBarberTypeId(hairResp.getBarberTypeId());
            hairResp.setBarberTypeName(coachType.getBarberTypeName());
            hairResp.setHairTypeName(hairType.getHairTypeName());
        }
        Collections.reverse(respList);
        return respList;
    }

    public PageResp<HairResp> list(HairReq req){
//        PageHelper.startPage(req.getPage(),req.getSize());
        //Page设置查询第几页,Size每页大小
        PageRequest pageRequest = PageRequest.of(req.getPage(), req.getSize());
        Page<Project> hairPage = projectRepository.findAll(pageRequest);
//        PageInfo<User>pageInfo=new PageInfo<>(userList);
        log.info("总数:{}",hairPage.getTotalElements());
        log.info("总页数:{}",hairPage.getTotalPages());
        //拷贝工具类
        List<HairResp> respList = CopyUtil.copyList(hairPage.getContent(), HairResp.class);
        for (HairResp hairResp:respList){
            HairType hairType = hairTypeRepository.findByHairTypeId(hairResp.getHairTypeId());
            CoachType coachType =barberTypeRepository.findByBarberTypeId(hairResp.getBarberTypeId());
            hairResp.setBarberTypeName(coachType.getBarberTypeName());
            hairResp.setHairTypeName(hairType.getHairTypeName());
        }
        PageResp<HairResp> pageResp = new PageResp<>();
        pageResp.setTotal(hairPage.getTotalElements());
        pageResp.setList(respList);
        return pageResp;
    }

    /**
     * 新增/更新 项目
     * @param hairForm
     */
    public void save(HairForm hairForm){
        Project hair = CopyUtil.copy(hairForm, Project.class);
        if (ObjectUtils.isEmpty(hairForm.getHairId())){
            hair.setHairId(snowFlake.nextId());
            hair.setCreateTime(new Date(System.currentTimeMillis()));
            hair.setOrderCount(0);
            hair.setHairStatus(true);
            projectRepository.save(hair);
        }else {
            Project repository = projectRepository.findByHairId(hairForm.getHairId());
            //解决不更新字段时不被设置为null;
            if (repository==null){
                throw new BusinessException(BusinessExceptionCode.HAIR_ERROR);
            }
            CopyUtil.copyNullProperties(repository,hair);
            projectRepository.save(hair);
        }
    }

    public HairResp findById(Long hairId) {
        Project hair = projectRepository.findByHairId(hairId);
        if (!ObjectUtils.isEmpty(hair)){
            HairType hairType = hairTypeRepository.findByHairTypeId(hair.getHairTypeId());
            CoachType coachType =barberTypeRepository.findByBarberTypeId(hair.getBarberTypeId());
            HairResp hairResp = CopyUtil.copy(hair, HairResp.class);
            hairResp.setHairTypeName(hairType.getHairTypeName());
            hairResp.setBarberTypeName(coachType.getBarberTypeName());
            return hairResp;
        }else {
        return null;
        }
    }

    public void del(Long hairId) {
        projectRepository.deleteById(hairId);
    }

    public List<HairResp> hairListByHairType(Long hairTypeId) {
        List<Project> hairList = projectRepository.findByHairTypeId(hairTypeId);
        List<HairResp> hairRespList = hairToHairResp(hairList);
        return hairRespList;
//        List<HairResp> hairRespList = CopyUtil.copyList(hairList, HairResp.class);
//        for (HairResp hairResp:hairRespList){
//            HairType hairType = hairTypeRepository.findByHairTypeId(hairResp.getHairTypeId());
//            BarberType barberType=barberTypeRepository.findByBarberTypeId(hairResp.getBarberTypeId());
//            hairResp.setBarberTypeName(barberType.getBarberTypeName());
//            hairResp.setHairTypeName(hairType.getHairTypeName());
//        }
//        return hairRespList;
    }

    public List<HairResp> hairListByBarberType(Long barberTypeId) {
        List<Project> hairList = projectRepository.findByBarberTypeId(barberTypeId);
        List<HairResp> hairRespList = hairToHairResp(hairList);
        return hairRespList;
    }
    public List<HairResp> byHairName(String hairName) {
        List<Project> hairList = projectRepository.findByHairNameLike(hairName);
        List<HairResp> hairRespList = hairToHairResp(hairList);
        return hairRespList;
    }
    public List<HairResp> hairToHairResp(List<Project> hairList){
        List<HairResp> hairRespList = CopyUtil.copyList(hairList, HairResp.class);
        for (HairResp hairResp:hairRespList){
            HairType hairType = hairTypeRepository.findByHairTypeId(hairResp.getHairTypeId());
            CoachType coachType =barberTypeRepository.findByBarberTypeId(hairResp.getBarberTypeId());
            hairResp.setBarberTypeName(coachType.getBarberTypeName());
            hairResp.setHairTypeName(hairType.getHairTypeName());
        }
        return hairRespList;
    }


//    public List<HairResp> index() {
//        List<HairType> typeList = hairTypeRepository.findAll();
//        HairType hairType = typeList.get(0);
//        List<HairResp> hairRespList = hairListByHairType(hairType.getHairTypeId());
//        return hairRespList;
//    }
}
