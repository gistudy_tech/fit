package com.gxy.fitness.service;

import com.gxy.fitness.entity.Coach;
import com.gxy.fitness.entity.CoachType;
import com.gxy.fitness.entity.Project;
import com.gxy.fitness.exception.BusinessException;
import com.gxy.fitness.exception.BusinessExceptionCode;
import com.gxy.fitness.form.BarberForm;
import com.gxy.fitness.repository.CoachRepository;
import com.gxy.fitness.repository.BarberTypeRepository;
import com.gxy.fitness.repository.ProjectRepository;
import com.gxy.fitness.req.BarberReq;
import com.gxy.fitness.resp.CoachResp;
import com.gxy.fitness.resp.PageResp;
import com.gxy.fitness.utils.CopyUtil;
import com.gxy.fitness.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Date;
import java.util.List;


/**
 * @author GUO
 * @Classname BarberService
 * @Description TODO
 * @Date 2022/2/26 17:40
 */
@Slf4j
@Service
public class CoachService {
    @Autowired
    private CoachRepository coachRepository;
    @Autowired
    private BarberTypeRepository barberTypeRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private SnowFlake snowFlake;
    public PageResp<CoachResp> list(BarberReq req){
//        PageHelper.startPage(req.getPage(),req.getSize());
        //Page设置查询第几页,Size每页大小
        PageRequest pageRequest = PageRequest.of(req.getPage(), req.getSize());
        Page<Coach> barberPage = coachRepository.findAll(pageRequest);
//        PageInfo<User>pageInfo=new PageInfo<>(userList);
        log.info("总数:{}",barberPage.getTotalElements());
        log.info("总页数:{}",barberPage.getTotalPages());
        //拷贝工具类
        List<CoachResp> respList = CopyUtil.copyList(barberPage.getContent(), CoachResp.class);
        for (CoachResp coachResp :respList){
            CoachType coachType = barberTypeRepository.findByBarberTypeId(coachResp.getBarberTypeId());
            coachResp.setBarberTypeName(coachType.getBarberTypeName());
        }

        PageResp<CoachResp> pageResp = new PageResp<>();
        pageResp.setTotal(barberPage.getTotalElements());
        pageResp.setList(respList);
        return pageResp;
    }

    /**
     * 新增/更新 理发师
     * @param barberForm
     */
    public void save(BarberForm barberForm){
        Coach coach = CopyUtil.copy(barberForm, Coach.class);
        if (ObjectUtils.isEmpty(barberForm.getBarberId())){
            //新增
            coach.setBarberId(snowFlake.nextId());
            coach.setCreateTime(new Date(System.currentTimeMillis()));
            coach.setOrderCount(0);
            coach.setBarberStatus(true);
            coachRepository.save(coach);
        }else {
            //修改
            Coach repository = coachRepository.findByBarberId(barberForm.getBarberId());
            //解决不更新字段时不被设置为null;
            if (repository==null){
                throw new BusinessException(BusinessExceptionCode.BARBER_ERROR);
            }
            CopyUtil.copyNullProperties(repository, coach);
            coachRepository.save(coach);
        }
    }

    public CoachResp findById(Long barberId) {
        Coach coach = coachRepository.findByBarberId(barberId);
        if (!ObjectUtils.isEmpty(coach)){
            CoachType coachType = barberTypeRepository.findByBarberTypeId(coach.getBarberTypeId());
            CoachResp coachResp = CopyUtil.copy(coach, CoachResp.class);
            coachResp.setBarberTypeName(coachType.getBarberTypeName());
            return coachResp;
        }
        return null;
    }
    public List<Coach> findTypeId(Long barberTypeId) {
        List <Coach> coachList =   coachRepository.findByBarberTypeId(barberTypeId);
        return coachList;
    }

    public void del(Long barberId) {
        coachRepository.deleteById(barberId);
    }

    public List<Coach> findHairId(Long hairId) {
        Project hair = projectRepository.findByHairId(hairId);
        Long barberTypeId = hair.getBarberTypeId();
        List<Coach> coachList = findTypeId(barberTypeId);
        return coachList;
    }

//    public List<Barber> all() {
//        List<Barber> barberList = barberRepository.findAll();
//        return barberList;
//    }
}
