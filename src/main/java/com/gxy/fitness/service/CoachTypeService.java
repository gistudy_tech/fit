package com.gxy.fitness.service;

import com.gxy.fitness.entity.CoachType;
import com.gxy.fitness.exception.BusinessException;
import com.gxy.fitness.exception.BusinessExceptionCode;
import com.gxy.fitness.repository.CoachRepository;
import com.gxy.fitness.repository.BarberTypeRepository;
import com.gxy.fitness.req.BarberTypeReq;
import com.gxy.fitness.resp.CoachTypeResp;
import com.gxy.fitness.utils.CopyUtil;
import com.gxy.fitness.utils.SnowFlake;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.sql.Date;
import java.util.List;

/**
 * @author GUO
 * @Classname BarberTypeService
 * @Description TODO
 * @Date 2021/12/11 15:21
 */
@Slf4j
@Service
public class CoachTypeService {
    @Autowired
    private BarberTypeRepository barberTypeRepository;
    @Autowired
    private CoachRepository coachRepository;
    @Autowired
    private SnowFlake snowFlake;

    public List<CoachTypeResp> list(){
        List<CoachType> typeList = barberTypeRepository.findAll();
        List<CoachTypeResp> barberTypeList = CopyUtil.copyList(typeList, CoachTypeResp.class);
        //查询不同类型理发师成员
        for (CoachTypeResp typeResp:barberTypeList){
            int count = coachRepository.countByBarberTypeId(typeResp.getBarberTypeId());
            typeResp.setCount(count);
        }
        return barberTypeList;
    }
    /**
     * 新增/更新 理发师类型
     * @param typeReq
     */
    public void save(BarberTypeReq typeReq){
        CoachType coachType = CopyUtil.copy(typeReq, CoachType.class);
        if (ObjectUtils.isEmpty(typeReq.getBarberTypeId())){
            coachType.setBarberTypeId(snowFlake.nextId());
            coachType.setCreateTime(new Date(System.currentTimeMillis()));
//            barberType.setCreateTime(DateUtil.DateToSimple());
            barberTypeRepository.save(coachType);
        }else {
            CoachType repository = barberTypeRepository.findByBarberTypeId(typeReq.getBarberTypeId());
            //解决不更新字段时不被设置为null;
            if (repository==null){
                throw new BusinessException(BusinessExceptionCode.BARBER_TYPE_ERROR);
            }
            CopyUtil.copyNullProperties(repository, coachType);
            barberTypeRepository.save(coachType);
        }
    }
    public void del(Long barberTypeId){
        barberTypeRepository.deleteById(barberTypeId);
    }

}
