import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/views/login";
import home from "@/views/home";
import welcome from "@/views/welcome";
import list from "@/views/user/list";
import order from "@/views/user/order";
import barberTypeList from "@/views/barberType/list";
import coachList from "@/views/coach/list";
import barberAdd from "@/views/coach/add";
import barberEdit from "@/views/coach/edit"
import hairList from "@/views/project/hairList";
import hairType from "@/views/project/hairType";
import store from '@/store'
import storage from "@/store/storage";
import hairEdit from "@/views/project/hairEdit";
import hairAdd from "@/views/project/hairAdd";

import index from "@/views/welcome/index";
import classify from "@/views/page/classify";
import ulogin from "@/views/page/login";
import cart from "@/views/page/cart";
import projectDetail from "@/views/page/projectDetail"
import userOrder  from "@/views/page/userOrder"
import info  from "@/views/page/info"
Vue.use(VueRouter);


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/welcome',
            component: welcome,
        },
        {
            path: '/login',
            component: Login,
        },
        {
            path: '/welcome',
            name: 'welcome',
            component: welcome,
            redirect: '/index',
            children:
                [
                    {
                        path: '/userOrder',
                        name: 'userOrder',
                        component: userOrder,
                    },
                    {
                        path: '/info',
                        name: 'info',
                        component: info,
                    },
                    {
                        path: '/index',
                        name: 'index',
                        component: index
                    },
                    {
                        path: '/classify',
                        name: 'classify',
                        component: classify
                    },
                    {
                        path: '/ulogin',
                        name: 'ulogin',
                        component: ulogin
                    },
                    {
                        path: '/cart',
                        name: 'cart',
                        component: cart
                    },
                    {
                        name: 'projectDetail',
                        path: '/id=:id',
                        component: projectDetail,
                    },



                ]
        },
        {
            path: '/sys/home',
            component: home,
            redirect: '/sys/order',
            meta: {
                requireAuth: true,
            },
            children:
                [{
                    path: '/sys/order',
                    name: 'order',
                    component: order
                },
                    {
                        path: '/sys/list',
                        component: list
                    },

                    {
                        name: '/sys/add',
                        path: '/sys/barberAdd',
                        component: barberAdd
                    },
                    {
                        name: '/sys/hairAdd',
                        path: '/sys/hairAdd',
                        component: hairAdd
                    },
                    {
                        path: '/sys/hairList',
                        component: hairList
                    },
                    {
                        path: '/sys/hairType',
                        component: hairType
                    },
                    {
                        path: '/sys/coachList',
                        component: coachList
                    },
                    {
                        path: '/sys/barberTypeList',
                        component: barberTypeList
                    },
                    {
                        name: 'barberEdit',
                        path: '/sys/barberId=:id',
                        component: barberEdit,
                    },
                    {
                        name: 'hairEdit',
                        path: '/sys/hairId=:id',
                        component: hairEdit,
                    }
                ]
        },
    ]
});


// 设置路由守卫，在进页面之前，判断有token，才进入页面，否则返回登录页面
if (storage.get("token")) {
    store.commit("set_token", storage.get("token"));
}
router.beforeEach((to, form, next) => {
        //要不要对mate.loginRequire属性做监控拦截
        if (to.matched.some(r => r.meta.requireAuth)) {
            if (store.state.token) {
                next(); //有token,进行request请求，后台还会验证token
            } else {
                next({
                    path: "/login",
                    // 将刚刚要去的路由path（却无权限）作为参数，方便登录成功后直接跳转到该路由，这要进一步在登陆页面判断
                    query: {redirect: to.fullPath}
                });
            }
        } else {
            next(); //如果无需token,那么随它去吧
        }
    }
);
export default router
